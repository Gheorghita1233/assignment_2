
const FIRST_NAME = "Gheorghita";
const LAST_NAME = "Nicolae";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
    
    
    var emptyObj={} ;

    var obj={} ;

    obj.pageAccessCounter=function (name) {
        
        if (name !== undefined)
            {
            name=name.toLowerCase();
            if(emptyObj[name]===undefined)
            emptyObj[name] =1;
            else
            emptyObj[name] +=1;
            }
        else 
        {   
            if(emptyObj['home']===undefined)
            emptyObj['home'] =1;
            else
            emptyObj['home'] +=1;
            }
        }
    

    obj.getCache= function ()
    {
    return emptyObj;
    }
    
    return obj;

 
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

